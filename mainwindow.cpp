#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    myWgt = new MyPainter (this);
    myWgt->setMinimumSize(50,100);
    ui->verticalLayout->addWidget(myWgt);
    ui->pushButton_4->hide();
    connect(myWgt, SIGNAL(signal_cursor_change()), this, SLOT(slot_cursor_change()));
    connect(myWgt, SIGNAL(signal_selected_rod(int)), this, SLOT(slot_selected_rod(int)));
    ///////
    //    QLabel *tempLabel = new QLabel;
    //    tempLabel->setText("TjpJ1xtNEWtext.");
    //    tempLabel->setFixedSize(100,100);
    //    tempLabel->show();
    //    QFont tempFont;
    //    tempFont = tempLabel->font();
    //    tempFont.setPixelSize(100*0.8);
    //    tempLabel->setFont(tempFont);
    //    qDebug() << tempLabel->size();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_3_clicked() // сделать хорошо
{// заставить рисовать. проверь текст на отсутствие букв и других символов
    //    qDebug () << ui->lineEdit->text();
    //    qDebug () << ui->lineEdit_2->text();
    if (myWgt->selectedRod == -1)
    {
        Rod *itemRod = new Rod; //itemRod->Ll = 0; itemRod->Aa = 0; itemRod->Ee = 0; itemRod->Qq = 0; itemRod->Fl = 0; itemRod->Fr = 0;
        itemRod->Ll = ui->lineEdit->text().toFloat();
        itemRod->Aa = ui->lineEdit_2->text().toFloat();
        itemRod->Ee = ui->lineEdit_3->text().toFloat();
        itemRod->Qq = ui->lineEdit_4->text().toFloat();
        if (myWgt->vecRods.empty() == true)
            itemRod->Fl = ui->lineEdit_5->text().toFloat();
        else
        {
            myWgt->vecRods.last()->Fr += ui->lineEdit_5->text().toFloat();
            itemRod->Fl = myWgt->vecRods.last()->Fr;
        }
        itemRod->Fr = ui->lineEdit_6->text().toFloat();

        this->myWgt->vecRods.push_back(itemRod);
        //        qDebug() << this->myWgt->vecRods.size();
    }
    else
    {
        myWgt->vecRods.at(myWgt->selectedRod)->Ll = ui->lineEdit->text().toFloat();
        myWgt->vecRods.at(myWgt->selectedRod)->Aa = ui->lineEdit_2->text().toFloat();
        myWgt->vecRods.at(myWgt->selectedRod)->Ee = ui->lineEdit_3->text().toFloat();
        myWgt->vecRods.at(myWgt->selectedRod)->Qq = ui->lineEdit_4->text().toFloat();
        if (myWgt->selectedRod > 0)
        {
            myWgt->vecRods.at(myWgt->selectedRod -1)->Fr = ui->lineEdit_5->text().toFloat();
            myWgt->vecRods.at(myWgt->selectedRod)   ->Fl = ui->lineEdit_5->text().toFloat();
            //            myWgt->vecRods.at(myWgt->selectedRod -1)->Fr += ui->lineEdit_5->text().toFloat();
            //            myWgt->vecRods.at(myWgt->selectedRod)->Fl = myWgt->vecRods.at(myWgt->selectedRod -1)->Fr;
        }
        else
            myWgt->vecRods.at(myWgt->selectedRod)->Fl = ui->lineEdit_5->text().toFloat();
        if (myWgt->selectedRod+1 < myWgt->vecRods.size())
        {
            myWgt->vecRods.at(myWgt->selectedRod+1)->Fl = ui->lineEdit_6->text().toFloat();
            myWgt->vecRods.at(myWgt->selectedRod)  ->Fr = ui->lineEdit_6->text().toFloat();
            //            myWgt->vecRods.at(myWgt->selectedRod +1)->Fl += ui->lineEdit_6->text().toFloat();
            //            myWgt->vecRods.at(myWgt->selectedRod)->Fr = myWgt->vecRods.at(myWgt->selectedRod +1)->Fl;
        }
        else
            myWgt->vecRods.at(myWgt->selectedRod)->Fr = ui->lineEdit_6->text().toFloat();
    }



    myWgt->selectedRod = -1;
    ui->pushButton_4->hide();
    ui->pushButton_3->setText("сделать хорошо");

    this->myWgt->update();
}

void MainWindow::slot_cursor_change()
{
    ui->lineEdit->setText  (QString().setNum(myWgt->vecRods.at(myWgt->cursorOn)->Ll,'g',6));
    ui->lineEdit_2->setText(QString().setNum(myWgt->vecRods.at(myWgt->cursorOn)->Aa,'g',6));
    ui->lineEdit_3->setText(QString().setNum(myWgt->vecRods.at(myWgt->cursorOn)->Ee,'g',6));
    ui->lineEdit_4->setText(QString().setNum(myWgt->vecRods.at(myWgt->cursorOn)->Qq,'g',6));
    ui->lineEdit_5->setText(QString().setNum(myWgt->vecRods.at(myWgt->cursorOn)->Fl,'g',6));
    ui->lineEdit_6->setText(QString().setNum(myWgt->vecRods.at(myWgt->cursorOn)->Fr,'g',6));
    QString temp;
    temp.push_back("UL="); temp.push_back(QString().setNum(myWgt->vecRods.at(myWgt->cursorOn)->UuMOOOOVEL,'g',6));
    ui->label_7->setText(temp);
    temp.clear();
    temp.push_back("UR="); temp.push_back(QString().setNum(myWgt->vecRods.at(myWgt->cursorOn)->UuMOOOOVER,'g',6));
    ui->label_8->setText(temp);
    temp.clear();
    temp.push_back("NL="); temp.push_back(QString().setNum(myWgt->vecRods.at(myWgt->cursorOn)->NnL,'g',6));
    ui->label_9->setText(temp);
    temp.clear();
    temp.push_back("NR="); temp.push_back(QString().setNum(myWgt->vecRods.at(myWgt->cursorOn)->NnR,'g',6));
    ui->label_10->setText(temp);
    temp.clear();

}

void MainWindow::on_pushButton_4_clicked()
{
    if (myWgt->selectedRod != -1)
    {
        this->myWgt->vecRods.removeAt(this->myWgt->selectedRod);
        myWgt->selectedRod = -1;
        myWgt->repaint();
        ui->pushButton_4->hide();
        ui->pushButton_3->setText("сделать хорошо");
    }
}

void MainWindow::slot_selected_rod(int item)
{
    if (item != -1)
    {
        ui->pushButton_4->show();
        ui->pushButton_3->setText("Изменить");
        if (myWgt->vecRods.at(item)->zadelkaL == true)
            ui->pushButton_2->setText("| remove");
        else
            ui->pushButton_2->setText("| Left");
        if (myWgt->vecRods.at(item)->zadelkaR == true)
            ui->pushButton->setText("remove |");
        else
            ui->pushButton->setText("Right |");
    }
    else
    {
        ui->pushButton_4->hide();
        ui->pushButton_3->setText("сделать хорошо");
        ui->pushButton->setText("Right |");
        ui->pushButton_2->setText("| Left");
    }

}

void MainWindow::on_pushButton_2_clicked() // заделка
{
    myWgt->vecRods.at(myWgt->selectedRod)->zadelkaL = !myWgt->vecRods.at(myWgt->selectedRod)->zadelkaL; // инверсия
    myWgt->repaint();
    emit slot_selected_rod(myWgt->selectedRod);
}

void MainWindow::on_pushButton_clicked() // заделка
{
    myWgt->vecRods.at(myWgt->selectedRod)->zadelkaR = !myWgt->vecRods.at(myWgt->selectedRod)->zadelkaR; // инверсия
    myWgt->repaint();
    emit slot_selected_rod(myWgt->selectedRod);
}
