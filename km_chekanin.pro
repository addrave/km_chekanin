QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = km_chekanin
TEMPLATE = app
CONFIG += console // добавляет консоль


SOURCES += main.cpp\
        mainwindow.cpp \
    mypainter.cpp \
    calculation.cpp

HEADERS  += mainwindow.h \
    mypainter.h \
    calculation.h \
    rod.h

FORMS    += mainwindow.ui
